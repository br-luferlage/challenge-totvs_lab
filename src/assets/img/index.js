import ImageNotFound from './image-notfound.png';
import Logo from './logo-totvs.png';
import Tag from './tag.png';

export const Images = {
  ImageNotFound,
  Logo,
  Tag,
};

import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* Types & Action Creators */

const { Types, Creators } = createActions({
  signInRequest: null,
  signInSuccess: ['user'],
  signInFailure: ['error'],
  signOutRequest: null,
  signOutSuccess: null,
  signOutFailure: ['error'],
});

export const AuthTypes = Types;
export default Creators;

/* Initial State */

export const INITIAL_STATE = Immutable({
  isChecked: false,
  loggedIn: false,
  loading: false,
  user: null,
  error: null,
});

/* Reducers */

export const request = state => state.merge({ loading: true });
export const success = (state, { user }) =>
  state.merge({ loading: false, user, loggedIn: true });
export const failure = (state, { error }) =>
  state.merge({ loading: false, error, loggedIn: false });

export const requestOut = state => state.merge({ loading: true });
export const successOut = state =>
  state.merge({ loading: false, loggedIn: false });
export const failureOut = (state, { error }) =>
  state.merge({ loading: false, error, loggedIn: false });

/* Reducers to types */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.SIGN_IN_REQUEST]: request,
  [Types.SIGN_IN_SUCCESS]: success,
  [Types.SIGN_IN_FAILURE]: failure,
  [Types.SIGN_OUT_REQUEST]: requestOut,
  [Types.SIGN_OUT_SUCCESS]: successOut,
  [Types.SIGN_OUT_FAILURE]: failureOut,
});

import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* Types & Action Creators */

const { Types, Creators } = createActions({
  setDefaultState: null,
  addProductRequest: ['product'],
  addProductSuccess: ['data', 'urlImage'],
  addProductFailure: ['error'],
  getProductsRequest: null,
  getProductsSuccess: ['data'],
  getProductsFailure: ['error'],
  uploadImgRequest: ['img'],
  uploadImgSuccess: ['urlImage'],
  uploadImgFailure: ['error'],
  editProductRequest: ['id', 'url', 'product'],
  editProductSuccess: ['data', 'urlImage'],
  editProductFailure: ['error'],
  deleteProductRequest: ['id'],
  deleteProductSuccess: ['data'],
  deleteProductFailure: ['error'],
});

export const ProductTypes = Types;
export default Creators;

/* Initial State */

export const INITIAL_STATE = Immutable({
  loading: false,
  error: false,
  data: [],
  uploading: false,
  progress: 0,
  urlImage: '',
  successUpload: false,
});

/* Reducers */

export const setDefault = state => state.merge({ successUpload: false });

export const request = state => state.merge({ loading: true });
export const success = (state, { data, urlImage }) =>
  state.merge({ loading: false, data: [...state.data, data], urlImage });
export const failure = (state, { error }) =>
  state.merge({ loading: false, error });

export const getRequest = state => state.merge({ loading: true });
export const getSuccess = (state, { data }) =>
  state.merge({ loading: false, data });
export const getFailure = (state, { error }) =>
  state.merge({ loading: false, error });

export const editRequest = state => state.merge({ loading: true });
export const editSuccess = (state, { data, urlImage }) =>
  state.merge({ loading: false, data, urlImage });
export const editFailure = (state, { error }) =>
  state.merge({ loading: false, error });

export const deleteRequest = state => state.merge({ loading: true });
export const deleteSuccess = (state, { data }) =>
  state.merge({ loading: false, data });
export const deleteFailure = (state, { error }) =>
  state.merge({ loading: false, error });

export const uploadRequest = state =>
  state.merge({ uploading: true, successUpload: false });
export const uploadSuccess = (state, { urlImage }) =>
  state.merge({ uploading: false, urlImage, successUpload: true });
export const uploadFailure = (state, { error }) =>
  state.merge({ uploading: false, successUpload: false, error });

/* Reducers to types */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.SET_DEFAULT_STATE]: setDefault,
  [Types.ADD_PRODUCT_REQUEST]: request,
  [Types.ADD_PRODUCT_SUCCESS]: success,
  [Types.ADD_PRODUCT_FAILURE]: failure,
  [Types.GET_PRODUCTS_REQUEST]: getRequest,
  [Types.GET_PRODUCTS_SUCCESS]: getSuccess,
  [Types.GET_PRODUCTS_FAILURE]: getFailure,
  [Types.EDIT_PRODUCT_REQUEST]: editRequest,
  [Types.EDIT_PRODUCT_SUCCESS]: editSuccess,
  [Types.EDIT_PRODUCT_FAILURE]: editFailure,
  [Types.DELETE_PRODUCT_REQUEST]: deleteRequest,
  [Types.DELETE_PRODUCT_SUCCESS]: deleteSuccess,
  [Types.DELETE_PRODUCT_FAILURE]: deleteFailure,
  [Types.UPLOAD_IMG_REQUEST]: uploadRequest,
  [Types.UPLOAD_IMG_SUCCESS]: uploadSuccess,
  [Types.UPLOAD_IMG_FAILURE]: uploadFailure,
});

import { call, put } from 'redux-saga/effects';
import AsyncStorage from '@react-native-community/async-storage';

import {
  GoogleSignin,
  statusCodes,
} from '@react-native-community/google-signin';

import { navigate } from '~/navigation/navigationservices';
import Routes from '~/navigation/routes';

import AuthActions from '../ducks/auth';

export function* checkLogged() {
  try {
    const logged = yield call([AsyncStorage, 'getItem'], '@auth');
    if (logged) {
      const user = JSON.parse(logged);
      yield put(AuthActions.signInSuccess(user));
    }
  } catch (error) {}
}

export function* signIn() {
  try {
    yield GoogleSignin.hasPlayServices({ showPlayServicesUpdateDialog: true });
    console.log('USER_INFO', userInfo);

    const userInfo = yield GoogleSignin.signIn();
    console.log('USER_INFO', userInfo);

    yield call([AsyncStorage, 'setItem'], '@auth', JSON.stringify(userInfo));
    yield put(AuthActions.signInSuccess(userInfo));

    navigate(Routes.HOME);
  } catch (error) {
    console.log('ERROR', error);
    if (error.code === statusCodes.SIGN_IN_CANCELLED) {
      // user cancelled the login flow
      navigate(Routes.MAIN);
    } else if (error.code === statusCodes.IN_PROGRESS) {
      // operation (e.g. sign in) is in progress already
    } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
      // play services not available or outdated
      navigate(Routes.MAIN);
    } else {
      // some other error happened
    }
  }
}

export function* signOut() {
  try {
    const loggedIn = yield call([AsyncStorage, 'getItem'], '@auth');
    if (loggedIn) {
      yield call([AsyncStorage, 'removeItem'], '@auth');
      yield GoogleSignin.revokeAccess();
      yield GoogleSignin.signOut();
      yield put(AuthActions.signOutSuccess());
    }
  } catch (error) {
    console.error(error);
    yield put(AuthActions.signOutFailure());
  }
}

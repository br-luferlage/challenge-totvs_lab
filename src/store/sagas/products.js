/* eslint-disable promise/always-return */
import { put, select } from 'redux-saga/effects';

import firestore from '@react-native-firebase/firestore';

import { navigate } from '~/navigation/navigationservices';
import Routes from '~/navigation/routes';

import { uploadImage } from '~/utils/upload';

import ProductsActions from '../ducks/products';

export function* addProduct({ product }) {
  try {
    let data = {};
    let urlImage = yield select(state => state.products.urlImage);

    const refProduct = yield firestore()
      .collection('products')
      .add({ ...product, urlImage });

    yield refProduct.get().then(doc => {
      refProduct.update({
        id: doc.id,
      });
    });

    yield refProduct.get().then(doc => {
      data = doc.data();
    });

    yield put(ProductsActions.addProductSuccess(data, (urlImage = '')));
    navigate(Routes.HOME);
  } catch (error) {
    yield put(ProductsActions.addProductFailure(error));
  }
}

export function* uploadImg({ img }) {
  try {
    const url = yield uploadImage(img);
    yield put(ProductsActions.uploadImgSuccess(url));
  } catch (error) {
    yield put(ProductsActions.uploadImgFailure(error));
  }
}

export function* getProducts() {
  try {
    const refProducts = yield firestore()
      .collection('products')
      .get();

    const data = refProducts.docs
      .map(doc => doc.data())
      .map(product => product);

    yield put(ProductsActions.getProductsSuccess(data));
  } catch (error) {
    yield put(ProductsActions.getProductsFailure(error));
  }
}

export function* editProduct({ id, url, product }) {
  try {
    let urlImage = yield select(state => state.products.urlImage);

    const doc = {
      ...product,
      id,
      urlImage: urlImage || url,
    };

    const response = yield firestore()
      .doc(`products/${id}`)
      .update(doc);

    const data = yield select(state => state.products.data);

    const productIndex = data.findIndex(p => p.id === id);
    const result = Object.assign([...data], {
      [productIndex]: doc,
    });

    yield put(ProductsActions.editProductSuccess(result, (urlImage = '')));
    navigate(Routes.HOME);
  } catch (error) {
    console.log('ERROR', error);
    yield put(ProductsActions.editProductFailure(error));
  }
}

export function* deleteProduct({ id }) {
  try {
    yield firestore()
      .doc(`products/${id}`)
      .delete();

    const data = yield select(state => state.products.data);

    const result = [...data.filter(p => p.id !== id)];

    yield put(ProductsActions.deleteProductSuccess(result));
    navigate(Routes.HOME);
  } catch (error) {
    yield put(ProductsActions.deleteProductFailure(error));
  }
}

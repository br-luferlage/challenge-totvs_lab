import { all, takeLatest } from 'redux-saga/effects';

import { AuthTypes } from '~/store/ducks/auth';
import { checkLogged, signIn, signOut } from './auth';

import { ProductTypes } from '~/store/ducks/products';
import {
  addProduct,
  getProducts,
  uploadImg,
  editProduct,
  deleteProduct,
} from './products';

export default function* rootSaga() {
  return yield all([
    checkLogged(),
    takeLatest(AuthTypes.SIGN_IN_REQUEST, signIn),
    takeLatest(AuthTypes.SIGN_OUT_REQUEST, signOut),
    takeLatest(ProductTypes.ADD_PRODUCT_REQUEST, addProduct),
    takeLatest(ProductTypes.EDIT_PRODUCT_REQUEST, editProduct),
    takeLatest(ProductTypes.DELETE_PRODUCT_REQUEST, deleteProduct),
    takeLatest(ProductTypes.GET_PRODUCTS_REQUEST, getProducts),
    takeLatest(ProductTypes.UPLOAD_IMG_REQUEST, uploadImg),
  ]);
}

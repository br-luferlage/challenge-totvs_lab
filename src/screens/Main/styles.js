import styled from 'styled-components/native';
import Button from '~/components/Button';

export const Container = styled.View`
  padding: 20px;
  flex: 1;
  justify-content: center;
`;

export const WrapperTop = styled.View`
  align-items: center;
  flex: 1;
  justify-content: flex-end;
`;

export const Img = styled.Image``;

export const WrapperTitle = styled.View`
  margin-top: 30;
`;

export const Title = styled.Text`
  text-align: center;
  font-size: 16;
  font-weight: 600;
`;

export const WrapperBottom = styled.View`
  padding: 0px 30px;
  flex: 1;
  justify-content: center;
`;

export const ButtonGoogle = styled(Button)``;

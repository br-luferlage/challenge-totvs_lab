import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { GoogleSignin } from '@react-native-community/google-signin';
import LoadingIndicator from '~/components/LoadingIndicator';
import { Images } from '~/assets/img';

import AuthActions from '~/store/ducks/auth';

import {
  Container,
  WrapperTop,
  Img,
  WrapperTitle,
  Title,
  WrapperBottom,
  ButtonGoogle,
} from './styles';

export default function Main({ navigation }) {
  const dispatch = useDispatch();
  const loading = useSelector(state => state.auth.loading);

  /**
   * Set up webClientId Google Sign In
   */
  useEffect(() => {
    async function bootstrap() {
      await GoogleSignin.configure({
        webClientId:
          '154860251183-gurtf18jbga43kns3p7pajummoukj43n.apps.googleusercontent.com',
      });
    }

    bootstrap();
  }, []);

  if (loading) {
    return <LoadingIndicator />;
  }

  return (
    <Container>
      <WrapperTop>
        <Img source={Images.Tag} resizeMode="contain" />

        <WrapperTitle>
          <Title>Welcome to our {'\n'} Mobile Coding Challenge</Title>
        </WrapperTitle>
      </WrapperTop>
      <WrapperBottom>
        <ButtonGoogle
          description="Login with google"
          onPress={() => dispatch(AuthActions.signInRequest())}
          icon="google"
          iconSize={25}
          colorIcon="#FFF"
          type="tertiary"
        />
      </WrapperBottom>
    </Container>
  );
}

/* eslint-disable react-hooks/exhaustive-deps */
import * as yup from 'yup';
import { Formik } from 'formik';
import React, { useState } from 'react';
import { useNavigationParam } from 'react-navigation-hooks';
import { useDispatch, useSelector } from 'react-redux';
import ImagePicker from 'react-native-image-picker';
import { Alert } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import LoadingIndicator from '~/components/LoadingIndicator';
import Button from '~/components/Button';
import TextInput from '~/components/TextInput';
import ButtonLeft from '~/components/Header';

import {
  ContainerPicture,
  Container,
  ButtonPicture,
  ButtonPictureText,
  WrapperForm,
  WrapperHorizontalForm,
  CustomTextInput,
  Img,
  WrapperButton,
} from './styles';

import ProductsActions from '~/store/ducks/products';

export default function Product() {
  const dispatch = useDispatch();
  const loading = useSelector(state => state.products.loading);
  const uploading = useSelector(state => state.products.uploading);
  const successUpload = useSelector(state => state.products.successUpload);
  const [imagePic, setImagePic] = useState();
  const product = useNavigationParam('product');

  /**
   * Function used to add a new product or edit an existing product
   * @param values - Data provided by the user
   */
  function handleSave(values) {
    product.id === ''
      ? imagePic
        ? dispatch(ProductsActions.addProductRequest(values))
        : Alert.alert('Imagem obrigatória', 'Por favor escolha uma imagem.', [
            { text: 'OK', onPress: () => takePicture() },
          ])
      : dispatch(
          ProductsActions.editProductRequest(
            product.id,
            product.urlImage,
            values,
          ),
        );
  }

  /**
   * Function used to capture a photo
   */
  function takePicture() {
    const options = {
      quality: 0.5,
      maxWidth: 200,
      maxHeight: 200,
      storageOptions: {
        skipBackup: true,
      },
    };

    ImagePicker.launchCamera(options, response => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = { uri: response.uri };

        setImagePic(source);

        /** Upload image to storage */
        dispatch(ProductsActions.uploadImgRequest(source));
      }
    });
  }

  return (
    <>
      <KeyboardAwareScrollView>
        <Container>
          {uploading ? (
            <ContainerPicture center>
              <LoadingIndicator />
            </ContainerPicture>
          ) : (
            <ContainerPicture>
              {imagePic ? (
                <Img source={imagePic} resizeMode="cover" />
              ) : (
                <Img source={{ uri: product.urlImage }} resizeMode="cover" />
              )}
              <ButtonPicture
                onPress={takePicture}
                hasPicture={imagePic || product.urlImage}
              >
                <ButtonPictureText hasPicture={imagePic || product.urlImage}>
                  {imagePic
                    ? 'EDIT PICTURE'
                    : product.urlImage
                    ? 'EDIT PICTURE'
                    : 'ADD PICTURE'}
                </ButtonPictureText>
              </ButtonPicture>
            </ContainerPicture>
          )}
          <Formik
            initialValues={{
              productName: product.productName,
              price: product.price,
              color: product.color,
              size: product.size,
            }}
            onSubmit={values => handleSave(values)}
            validationSchema={yup.object().shape({
              productName: yup.string().required('Product name is required'),
              price: yup.string().required('Price is required'),
              color: yup.string().required('Color is required'),
              size: yup.string().required('Size is required'),
            })}
          >
            {({ values, handleChange, errors, touched, handleSubmit }) => (
              <>
                <WrapperForm>
                  <TextInput
                    label="Product Name"
                    onChangeText={handleChange('productName')}
                    value={values.productName}
                    placeholder="Enter name"
                    autoCapitalize="none"
                    autoCorrect={false}
                    keyboardType="default"
                    touched={touched.productName}
                    errors={errors.productName}
                  />

                  <TextInput
                    label="Price"
                    onChangeText={handleChange('price')}
                    value={values.price}
                    placeholder="Enter price"
                    autoCapitalize="none"
                    autoCorrect={false}
                    keyboardType="default"
                    touched={touched.price}
                    errors={errors.price}
                    inputMask
                    typeMask="money"
                    optionsMask={{
                      precision: 2,
                      separator: ',',
                      delimiter: '.',
                      unit: '$',
                      suffixUnit: '',
                    }}
                  />

                  <WrapperHorizontalForm>
                    <CustomTextInput
                      label="Color"
                      onChangeText={handleChange('color')}
                      value={values.color}
                      placeholder="Enter color"
                      autoCapitalize="none"
                      autoCorrect={false}
                      keyboardType="default"
                      touched={touched.color}
                      errors={errors.color}
                    />

                    <CustomTextInput
                      label="Size"
                      onChangeText={handleChange('size')}
                      value={values.size}
                      placeholder="Enter size"
                      autoCapitalize="none"
                      autoCorrect={false}
                      keyboardType="default"
                      touched={touched.size}
                      errors={errors.size}
                    />
                  </WrapperHorizontalForm>
                </WrapperForm>
                <WrapperButton>
                  <Button
                    type="primary"
                    description="Save"
                    onPress={handleSubmit}
                    loading={loading}
                    disabled={product.id !== '' ? false : !successUpload}
                  />
                  {product.id !== '' && (
                    <Button
                      type="secondary"
                      description="Delete"
                      onPress={() =>
                        dispatch(
                          ProductsActions.deleteProductRequest(product.id),
                        )
                      }
                    />
                  )}
                </WrapperButton>
              </>
            )}
          </Formik>
        </Container>
      </KeyboardAwareScrollView>
    </>
  );
}

Product.navigationOptions = {
  headerLeft: <ButtonLeft />,
};

import styled from 'styled-components/native';
import TextInput from '~/components/TextInput';

export const Container = styled.View`
  flex: 1;
`;

export const ContainerPicture = styled.View`
  height: 200;
  background: rgb(210, 210, 211);
  align-items: ${props => (props.center ? 'center' : 'flex-start')};
  justify-content: ${props => (props.center ? 'center' : 'flex-start')};
`;

export const ButtonPicture = styled.TouchableOpacity`
  width: 169;
  height: 36;
  border: 1px;
  border-color: ${props => (props.hasPicture ? '#FFF' : '#000')}
  border-radius: 20;
  align-items: center;
  justify-content: center;
  position: absolute;
  top: 150;
  left: 220;
`;

export const ButtonPictureText = styled.Text`
  color: ${props => (props.hasPicture ? '#FFF' : '#000')};
`;

export const WrapperForm = styled.View`
  padding: 20px;
  flex: 1;
`;

export const Label = styled.Text`
  color: rgb(173, 173, 173);
  margin-bottom: 12;
`;

export const WrapperHorizontalForm = styled.View`
  flex-direction: row;
  justify-content: space-between;
`;

export const CustomTextInput = styled(TextInput)`
  width: 47%;
`;

export const Img = styled.Image`
  width: 100%;
  height: 100%;
`;

export const WrapperButton = styled.View`
  padding: 20px 30px;
`;

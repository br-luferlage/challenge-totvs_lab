/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import Routes from '~/navigation/routes';
import Button from '~/components/Button';
import LoadingIndicator from '~/components/LoadingIndicator';

import {
  Container,
  List,
  WrapperItem,
  WrapperImage,
  Img,
  WrapperItemData,
  ProductName,
  ProductPrice,
  WrapperDetails,
  WrapperColor,
  Label,
  ProductColor,
  WrapperSize,
  ProductSize,
  WrapperEditButton,
  CustomButton,
  TextButton,
  WrapperButton,
} from './styles';

import AuthActions from '~/store/ducks/auth';
import ProductsActions from '~/store/ducks/products';

export default function Home({ navigation }) {
  const dispatch = useDispatch();
  const products = useSelector(state => state.products.data);
  const loading = useSelector(state => state.products.loading);

  /**
   * Load product List,
   * @return {Object}
   */
  useEffect(() => {
    dispatch(ProductsActions.getProductsRequest());
  }, []);

  /**
   * Navigate to Add New Product Screen
   */
  function goToAddNewProduct() {
    navigation.navigate(Routes.PRODUCT, {
      product: {
        id: '',
        productName: '',
        price: '',
        color: '',
        size: '',
      },
    });
  }

  /**
   * Navigate to Edit Product Screen
   * @param product - The object containing the product to be edited
   */
  function goToEditProduct(product) {
    navigation.navigate(Routes.PRODUCT, {
      product,
    });
  }

  return (
    <Container>
      {console.log('loading', loading)}
      {loading ? (
        <LoadingIndicator />
      ) : (
        <>
          <List
            data={products}
            keyExtractor={product => product.id}
            renderItem={({ item }) => (
              <WrapperItem>
                <WrapperImage>
                  <Img source={{ uri: item.urlImage }} resizeMode="contain" />
                </WrapperImage>
                <WrapperItemData>
                  <ProductName>{item.productName}</ProductName>
                  <ProductPrice>RP {item.price}</ProductPrice>
                  <WrapperDetails>
                    <WrapperColor>
                      <Label>Color</Label>
                      <ProductColor>{item.color}</ProductColor>
                    </WrapperColor>
                    <WrapperSize>
                      <Label>Size</Label>
                      <ProductSize>{item.size}</ProductSize>
                    </WrapperSize>
                  </WrapperDetails>
                </WrapperItemData>
                <WrapperEditButton>
                  <CustomButton onPress={() => goToEditProduct(item)}>
                    <TextButton>Edit</TextButton>
                  </CustomButton>
                </WrapperEditButton>
              </WrapperItem>
            )}
            onEndReached={() => {}}
            onEndReachedThreshold={0.5}
          />

          <WrapperButton>
            <Button
              type="primary"
              description="Add a new product"
              onPress={goToAddNewProduct}
            />
            <Button
              type="secondary"
              description="Sign out"
              onPress={() => dispatch(AuthActions.signOutRequest())}
            />
          </WrapperButton>
        </>
      )}
    </Container>
  );
}

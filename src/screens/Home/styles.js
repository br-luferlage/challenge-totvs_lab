import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  background-color: #fff;
  padding: 10px;
`;

export const List = styled.FlatList.attrs({
  showsVerticalScrollIndicator: false,
})``;

export const WrapperImage = styled.View`
  align-items: center;
  justify-content: center;
`;

export const Img = styled.Image`
  width: 80;
  height: 100;
  border-radius: 10;
`;

export const WrapperItem = styled.View`
  flex-direction: row;
  justify-content: space-between;
  margin-bottom: 15;
  border-bottom-color: #ccc;
  border-bottom-width: 0.5;
  padding-bottom: 10;
`;

export const WrapperItemData = styled.View`
  padding: 10px 15px 0px 15px;
  flex: 1;
`;

export const ProductName = styled.Text.attrs({
  numberOfLines: 1,
})`
  font-size: 20;
  font-weight: 500;
  margin-bottom: 15;
  color: rgb(140, 140, 140);
  text-transform: uppercase;
`;

export const ProductPrice = styled.Text`
  font-size: 18;
  font-weight: 500;
  color: rgb(118, 163, 68);
`;

export const WrapperDetails = styled.View`
  flex-direction: row;
  margin-top: 12;
  margin-bottom: 10;
`;

export const WrapperColor = styled.View`
  flex: 1;
`;

export const Label = styled.Text`
  color: rgba(112, 112, 112, 0.8);
`;

export const ProductColor = styled.Text.attrs({
  numberOfLines: 1,
})`
  color: rgb(105, 105, 105);
  text-transform: uppercase;
`;

export const WrapperSize = styled.View`
  flex: 1;
`;

export const ProductSize = styled.Text.attrs({
  numberOfLines: 1,
})`
  color: rgb(105, 105, 105);
  text-transform: uppercase;
`;

export const WrapperEditButton = styled.View`
  padding-top: 10;
`;

export const CustomButton = styled.TouchableOpacity`
  border: 1px #707070;
  padding: 10px 25px 10px 25px;
  border-radius: 20;
`;

export const TextButton = styled.Text`
  font-size: 18;
  font-weight: 500;
  color: #707070;
`;

export const WrapperButton = styled.View`
  padding: 10px 30px;
`;

import React from 'react';
import { Provider } from 'react-redux';
import { Platform, StatusBar } from 'react-native';

import './config/ReactotronConfig';

import store from './store';
import App from './App';

console.disableYellowBox = true;

// TODO Fix network debugging layer on Android
if (Platform.OS === 'ios') {
  global.XMLHttpRequest = global.originalXMLHttpRequest
    ? global.originalXMLHttpRequest
    : global.XMLHttpRequest;

  global.FormData = global.originalFormData
    ? global.originalFormData
    : global.FormData;
}

export default function Index() {
  return (
    <Provider store={store}>
      <StatusBar barStyle="dark-content" backgroundColor="#fff" />
      <App />
    </Provider>
  );
}

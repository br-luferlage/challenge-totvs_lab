import React from 'react';
import { withNavigation } from 'react-navigation';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { Images } from '~/assets/img';
import { BackButton, WrapperLogo, Img } from './styles';

export const ButtonLeft = ({ navigation }) => {
  return (
    <BackButton
      onPress={() => {
        navigation.goBack();
      }}
    >
      <Icon name="arrow-back" size={30} color="#565656" />
    </BackButton>
  );
};

export const Logo = props => (
  <WrapperLogo>
    <Img source={Images.Logo} resizeMode="contain" />
  </WrapperLogo>
);

export default withNavigation(ButtonLeft);

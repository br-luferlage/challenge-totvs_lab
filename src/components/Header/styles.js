import styled from 'styled-components/native';

export const BackButton = styled.TouchableOpacity`
  margin-left: 10;
`;

export const WrapperLogo = styled.View`
  margin-right: 10;
`;

export const Img = styled.Image``;

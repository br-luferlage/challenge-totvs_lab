import React from 'react';
import { string, func, bool } from 'prop-types';

import {
  Container,
  Label,
  Input,
  InputMask,
  WrapperError,
  TextError,
} from './styles';

const InputText = ({
  style,
  label,
  onChangeText,
  value,
  placeholder,
  placeholderTextColor,
  autoCapitalize,
  autoCorrect,
  keyboardType,
  touched,
  errors,
  inputMask,
  typeMask,
  optionsMask,
  ref,
  onSubmitEditing,
}) => (
  <Container style={style}>
    <Label>{label}</Label>
    {inputMask ? (
      <InputMask
        type={typeMask}
        options={optionsMask}
        value={value}
        onChangeText={onChangeText}
        placeholder={placeholder}
        placeholderTextColor={placeholderTextColor}
        autoCapitalize={autoCapitalize}
        autoCorrect={autoCorrect}
        keyboardType={keyboardType}
        ref={ref}
        onSubmitEditing={onSubmitEditing}
      />
    ) : (
      <Input
        onChangeText={onChangeText}
        value={value}
        placeholder={placeholder}
        placeholderTextColor={placeholderTextColor}
        autoCapitalize={autoCapitalize}
        autoCorrect={autoCorrect}
        keyboardType={keyboardType}
        ref={ref}
        onSubmitEditing={onSubmitEditing}
      />
    )}

    <WrapperError>
      {touched && errors && <TextError>{errors}</TextError>}
    </WrapperError>
  </Container>
);

InputText.propTypes = {
  style: string,
  label: string.isRequired,
  onChangeText: func.isRequired,
  value: string.isRequired,
  placeholder: string.isRequired,
  placeholderTextColor: string,
  autoCapitalize: string,
  autoCorrect: bool,
  keyboardType: string,
  touched: string.isRequired,
  errors: string.isRequired,
  inputMask: bool,
  typeMask: string,
  optionsMask: string,
  ref: string,
  onSubmitEditing: func,
};

InputText.defaultProps = {
  style: null,
  placeholderTextColor: '#ccc',
  autoCapitalize: 'none',
  autoCorrect: false,
  keyboardType: 'default',
  inputMask: false,
  typeMask: null,
  optionsMask: null,
  ref: null,
  onSubmitEditing: () => {},
};

export default InputText;

import styled from 'styled-components/native';
import { TextInputMask } from 'react-native-masked-text';

export const Container = styled.View`
  margin: 10px 0px 10px 0px;
`;

export const Label = styled.Text`
  color: rgb(173, 173, 173);
  margin-bottom: 12;
`;

export const Input = styled.TextInput`
  font-size: 16;
  font-weight: 500;
  justify-content: center;
  border-width: 1;
  padding: 20px;
  border-radius: 4;
  border-color: rgb(194, 200, 199);
  color: #000;
`;

export const InputMask = styled(TextInputMask)`
  font-size: 16;
  font-weight: 500;
  justify-content: center;
  border-width: 1;
  padding: 20px;
  border-radius: 4;
  border-color: rgb(194, 200, 199);
  color: #000;
`;

export const WrapperError = styled.View`
  align-items: center;
`;

export const TextError = styled.Text`
  font-size: 16;
  color: red;
  margin-top: 5;
`;

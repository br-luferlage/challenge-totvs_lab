import styled from 'styled-components/native';
import FAIcon from 'react-native-vector-icons/FontAwesome';

export const Container = styled.TouchableOpacity`
  height: 60;
  border-width: 1;
  border-color: ${props =>
    props.type === 'primary'
      ? '#FFF'
      : props.type === 'secondary'
      ? 'transparent'
      : props.type === 'tertiary'
      ? 'transparent'
      : 'transparent'};
  background-color: ${props =>
    props.type === 'primary'
      ? 'rgb(118, 163, 68)'
      : props.type === 'secondary'
      ? 'transparent'
      : props.type === 'tertiary'
      ? 'rgb(177,73,61)'
      : 'transparent'};
  border-radius: 10;
  align-items: center;
  justify-content: center;
  flex-direction: row;
  opacity: ${props => (props.disabled ? 0.5 : 1)};
`;

export const Icon = styled(FAIcon)``;

export const WrapperDescription = styled.View`
  flex: 1;
  max-width: 80%;
`;

export const Description = styled.Text`
  font-size: 18;
  color: ${props =>
    props.type === 'primary'
      ? '#FFF'
      : props.type === 'secondary'
      ? '#000'
      : '#FFF'};
  text-align: center;
`;

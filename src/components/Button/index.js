import React from 'react';
import { string, func, number, bool } from 'prop-types';

import LoadingIndicator from '~/components/LoadingIndicator';

import { Container, WrapperDescription, Description, Icon } from './styles';

const Button = ({
  type,
  fontsize,
  icon,
  iconSize,
  colorIcon,
  description,
  style,
  onPress,
  loading,
  disabled,
}) => (
  <Container type={type} style={style} onPress={onPress} disabled={disabled}>
    {loading ? (
      <LoadingIndicator />
    ) : (
      <>
        {icon && <Icon name={icon} size={iconSize} color={colorIcon} />}
        <WrapperDescription>
          <Description type={type} size={fontsize} style={style}>
            {description}
          </Description>
        </WrapperDescription>
      </>
    )}
  </Container>
);

Button.propTypes = {
  type: string,
  fontsize: string,
  icon: string,
  iconSize: number.isRequired,
  colorIcon: string,
  style: string,
  description: string,
  onPress: func,
  loading: string,
  disabled: bool,
};

Button.defaultProps = {
  type: 'primary',
  fontsize: 'default',
  icon: null,
  colorIcon: '#FFF',
  style: {},
  description: 'Informe o texto do botao',
  loading: false,
  disabled: false,
  onPress: () => {},
};

export default Button;

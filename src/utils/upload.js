import { firebase } from '@react-native-firebase/storage';
import { format } from 'date-fns';

export async function uploadImage(img) {
  // Extract image extension
  const ext = img.uri.split('.').pop();

  // Strategy to export filename
  const prepareName = new Date();
  const formattedName = format(prepareName, 'yyyy-MM-dd hh:mm:ss');
  const filename = `${formattedName}.${ext}`;

  // Create image to firebase/storage
  await firebase
    .storage()
    .ref(`products/${filename}`)
    .putFile(img.uri);

  // Retrieve image from firebase/storage
  const ref = firebase.storage().ref(`products/${filename}`);
  const urlCloudStorage = await ref.getDownloadURL();

  return urlCloudStorage;
}

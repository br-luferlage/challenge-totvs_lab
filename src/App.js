import React from 'react';
import { useSelector } from 'react-redux';

import { setTopLevelNavigator } from '~/navigation/navigationservices';
import createRouter from '~/navigation';

export default function App() {
  const signed = useSelector(state => state.auth.loggedIn);
  const Routes = createRouter(signed);
  return <Routes ref={setTopLevelNavigator} />;
}

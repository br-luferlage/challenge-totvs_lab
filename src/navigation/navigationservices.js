import { NavigationActions } from 'react-navigation';

let nav;

export function setTopLevelNavigator(navigatorRef) {
  nav = navigatorRef;
}

export function navigate(routeName, params) {
  nav.dispatch(NavigationActions.navigate({ routeName, params }));
}

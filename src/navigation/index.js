import React from 'react';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';

import { createStackNavigator } from 'react-navigation-stack';

import Routes from './routes';

import { Logo } from '~/components/Header';

import Main from '~/screens/Main';
import Home from '~/screens/Home';
import Product from '~/screens/Product';

const OnboardingStack = createStackNavigator(
  {
    [Routes.MAIN]: {
      screen: Main,
    },
  },
  {
    initialRouteName: Routes.MAIN,
    mode: 'card',
    headerLayoutPreset: 'center',
    defaultNavigationOptions: {
      headerRight: <Logo />,
      headerTintColor: '#fff',
      headerStyle: {
        borderBottomWidth: 0,
        height: 60,
      },
    },
  },
);

const MainStack = createStackNavigator(
  {
    [Routes.HOME]: {
      screen: Home,
    },
    [Routes.PRODUCT]: {
      screen: Product,
    },
  },

  {
    initialRouteName: Routes.HOME,
    mode: 'card',
    headerLayoutPreset: 'center',
    defaultNavigationOptions: {
      headerRight: <Logo />,
      headerTintColor: '#fff',
      headerStyle: {
        borderBottomWidth: 0,
        height: 60,
      },
    },
  },
);

export default (signedIn = false) =>
  createAppContainer(
    createSwitchNavigator(
      {
        Sign: createSwitchNavigator({
          [Routes.ONBOARDING]: OnboardingStack,
        }),
        App: createSwitchNavigator({
          [Routes.HOME]: MainStack,
        }),
      },
      {
        initialRouteName: signedIn ? 'App' : 'Sign',
      },
    ),
  );

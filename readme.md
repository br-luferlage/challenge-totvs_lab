# Challenge TotvsLabs

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

`yarn, xcode, cocoapods, android sdk + android simulator`

### Installing

Enter the project folder and execute the following commands in the terminal:

`yarn, cd ios && pod install`

## Author

- **Luiz Fernando Fonseca** luferlage@gmail.com - +55 24 999 510 066
